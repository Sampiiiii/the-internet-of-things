
# ProUpdThing ################################################################

A provisioning and OTA update example.

**Note**: HTTPS root certificate checking as used here doesn't work in IDF
4.2.

The code is in `main/`; an Arduino-ified version is in `sketch/`; build magic
happens in `magic.sh`; the build script will install the SDKs for you (in
`~/esp`, linked from`local-sdks/`) if you ask it nicely (and if you're running
a recent Ubuntu or RaspiOS).

A minimal install, build, burn to device and then monitor would look like
this (plug in your ESP32 Feather first!):

```bash
pio run -t upload -t monitor
```

(using [PlatformIO core](https://platformio.org/install/cli)), or like this:

```bash
./magic.sh idf-install
./magic.sh
```

(using IDF with the Arduino core as a component).

For help see:

```bash
./magic.sh -h
```

If you want to access other `idf.py` commands, try

```bash
./magic.sh idf-py your-favourite-command
```

E.g.: `./magic.sh idf-py menuconfig` to reconfigure the SDK (and update
`sdkconfig`).

Files:

- `magic.sh`:           build script; try `./magic.sh build-help`
- `main`:               source code
  - `main.cpp`:         main entry points: `setup`, `loop`, `app_main`
  - `sketch.h`:         defines a macro for Arduino IDE builds
  - `CMakeLists.txt`:   CMake build config
- `partitions.csv`:     partitioning scheme
- `sdkconfig.defaults`: configuration defaults
- `sketch`:             links to `main` that facilitate Arduino IDE compiles
  - `sketch.ino`:       links `../main/main.cpp`
  - `sketch.h`:         defines a macro for Arduino IDE builds
- `.gitignore`:         git config
- `CMakeLists.txt`:     CMake build config

Generated files:

- `build`:              generated build files
- `local-sdks`:         the SDKs
                        (ESP IDF, the ESP32 Arduino core, RainMaker, etc.)
- `sdkconfig`:          configuration; incorporates defaults from
                        `sdkconfig.defaults`
