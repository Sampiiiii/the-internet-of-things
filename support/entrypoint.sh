#!/bin/bash
# entrypoint.sh

# environment
echo entrypoint...
source /home/ubuntu/.bashrc
source /home/ubuntu/the-internet-of-things/support/tooling/platformio/penv/bin/activate
echo "\$0 = $0"
echo "\$* = $*"
echo "PATH = $PATH"
echo "MAGIC_COM = $MAGIC_COM"

# for convenience let's pull latest git repos
echo "pulling latest from git repos..."
(cd the-internet-of-things; git pull)
(cd unphone; git pull)
(cd unPhoneLibrary; git pull)

# uncomment to use latest dev release of platformio
#echo "upgrading to latest platformio..."
#pio upgrade --dev

# run a shell or a magic.sh command if we have one
cd project && pwd || :
if [ -z "$MAGIC_COM" ]
then
  echo 'running shell'
  bash
else
  echo 'running magic.sh'
  ~/the-internet-of-things/support/magic.sh $*
fi

# ensure all mapped files are writeable back on the host (ignoring big dirs)
# TODO parameterise this
cd $HOME
[ "$(find project |wc -l)" -gt 20000 ] && { echo 'big dir, no chmod'; exit 0; }
chmod -R g+rw project                                   2>/dev/null
chmod    g+s  `find project -type d 2>/dev/null`        2>/dev/null
